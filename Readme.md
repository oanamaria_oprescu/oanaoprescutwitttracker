//Install
Create tweets database
tweets table will create automatically
predefined auth to db is: localhost, root, {empty password}

//USAGE
1:'welcome! <br /> please, create tweets database in mysql',
      2:'go to localhost:8080/parse_tweet_stream',
      3:'this will start saving the twitter stream to DB',
      4: 'kill server process to stop stream',
      5: 'go to http://localhost:8080/parse_tweet_stream to start inserting tweets to database',
      6: 'go to http://localhost:8080/tweets to read tweets database',
      7: 'go to /tweet/:id to read specific tweet',
      8: 'Check server.js for post methods'