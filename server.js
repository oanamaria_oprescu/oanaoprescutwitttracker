const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const request = require('request');
const util = require('util');



//Define Twitter API
//promisify pachet 
const get = util.promisify(request.get);
const post = util.promisify(request.post);

const consumer_key = 'BOX90uMNmD9wLWTo7WeEu3Cqq'; 
const consumer_secret = 'aXjnDq7CPo6uc2NFsNZGilKXsG4XNHpDqxtjsHNW3sCTi2Tyrg'; 

const bearerTokenURL = new URL('https://api.twitter.com/oauth2/token'); // api auth twitter
const streamURL = new URL('https://api.twitter.com/labs/1/tweets/stream/filter'); // api read stream
const rulesURL = new URL('https://api.twitter.com/labs/1/tweets/stream/filter/rules'); // api set rules for stream

//Auth with Twitter using a Token 
// Face post catre url si trimite user si pass ca sa primim un Token de autentificare
async function bearerToken (auth) {
  const requestConfig = {
    url: bearerTokenURL,
    auth: {
      user: consumer_key,
      pass: consumer_secret,
    },
    form: {
      grant_type: 'client_credentials',
    },
  };

    //Aici se trimit prin POST requestConfig de mai sus 
  const response = await post(requestConfig);
  return JSON.parse(response.body).access_token;
}

async function getAllRules(token) {
  const requestConfig = {
    url: rulesURL,
    auth: {
      bearer: token
    }
  };

  const response = await get(requestConfig);
  if (response.statusCode !== 200) {
    throw new Error(response.body);
    return null;
  }

  return JSON.parse(response.body);
}

async function deleteAllRules(rules, token) {
  if (!Array.isArray(rules.data)) {
    return null;
  }

  const ids = rules.data.map(rule => rule.id);

  const requestConfig = {
    url: rulesURL,
    auth: {
      bearer: token
    },
    json: {
      delete: {
        ids: ids
      }
    }
  };

  const response = await post(requestConfig);
  if (response.statusCode !== 200) {
    throw new Error(JSON.stringify(response.body));
    return null;
  }

  return response.body;
}

async function setRules(rules, token) {
  const requestConfig = {
    url: rulesURL,
    auth: {
      bearer: token
    },
    json: {
      add: rules  
    }
  };

    //Fiind functie asincrona, putem astepta POST catre rulesURL cu param Token
  const response = await post(requestConfig);
  if (response.statusCode !== 201) {
    throw new Error(JSON.stringify(response.body));
    //Return null daca avem eroare;
    return null;
  }
  //Altfel, returnam raspunsul
  return response.body;
}


function streamConnect(token) {

	console.log('connectiong to stream...');
	console.log('Please be patient...');

  // Listen to the stream
  const config = {
    url: 'https://api.twitter.com/labs/1/tweets/stream/filter?format=compact',
    auth: {
      bearer: token,
    },
    timeout: 20000,
  };

  const stream = request.get(config);

  console.log('config done');

  stream.on('data', data => {
      try {
        const json = JSON.parse(data);
       // console.log(json);


        console.log('data here: '+json);

        console.log(json.data.text);

      
        //Sequelize insert Tweet in database
        Tweet.create({ text: json.data.text }).then(function(user) {
          // you can now access the newly created user
          console.log('success', user.toJSON());
      })
      .catch(function(err) {
          // print the error details
          console.log(err, request.body);
      });


      } catch (e) {
        console.log('error: '+e);
      }

      
  }).on('error', error => {
    if (error.code === 'ETIMEDOUT') {
      stream.emit('timeout');
    }
  });


  return stream;
}

//End Twitter API Definition

//Require mysql module & connect to DB

var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "tweets"
});

//Create table if not exists
con.connect(function(err) {
  if (err) throw err;
  console.log("Connected!");
  var sql = "CREATE TABLE IF NOT EXISTS `tweets` (    `ID` int(11) NOT NULL AUTO_INCREMENT,    `text` text NOT NULL,    `created_at` datetime NOT NULL DEFAULT current_timestamp(),    PRIMARY KEY (`ID`)  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
  con.query(sql, function (err, result) {
    if (err) throw err;
    console.log("Table created");
  });
});




//sequlize def

const sequelize = new Sequelize('tweets','root','',{
	dialect : 'mysql',
	define : {
		timestamps : false
	}
})

//def ob de tip tabela
const Tweet = sequelize.define('tweet', {
	text : Sequelize.TEXT
})




//Start Express def
//body-parser primesc json si il transf in obj
const app = express()
app.use(bodyParser.json())
app.use(express.static('../simple-app/build'))

// app.get('/create', async (req, res) => {
// 	try{
// 		await sequelize.sync({force : true})
// 		res.status(201).json({message : 'created'})
// 	}
// 	catch(e){
// 		console.warn(e)
// 		res.status(500).json({message : 'server error'})
// 	}
// })

//Homepage
app.get('/', async (req, res) => {
	try{
		let info = {

      1:'welcome! <br /> please, create tweets database in mysql',
      2:'go to localhost:8080/parse_tweet_stream',
      3:'this will start saving the twitter stream to DB',
      4: 'kill server process to stop stream',
      5: 'go to http://localhost:8080/parse_tweet_stream to start inserting tweets to database',
      6: 'go to http://localhost:8080/tweets to read tweets database',
      7: 'go to /tweet/:id to read specific tweet',
      8: 'Check server.js for post methods'


    }
		res.status(200).json(info)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

//API READ DATABASE FOR TWEETS
app.get('/tweets', async (req, res) => {
	try{
		let tweets = await Tweet.findAll()
		res.status(200).json(tweets)
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

//Start Stream and insert tweets to Database
//Kill process to stop stream
app.get('/parse_tweet_stream', async (req, res) => {

	console.log('starting stream');
	console.log('end it by closing server');
	
	(async () => {
  let token, currentRules;
  //APPLY RULES FOR COMPANY
  const rules = [

    { 'value': 'company' },
  ];

  try {
    // Exchange your credentials for a Bearer token
    token = await bearerToken({consumer_key, consumer_secret});
  } catch (e) {
    console.error(`Could not generate a Bearer token. Please check that your credentials are correct and that the Filtered Stream preview is enabled in your Labs dashboard. (${e})`);
    process.exit(-1);
  }

  try {
    // Gets the complete list of rules currently applied to the stream
    currentRules = await getAllRules(token);
    
    // Delete all rules. Comment this line if you want to keep your existing rules.
    await deleteAllRules(currentRules, token);

    // Add rules to the stream. Comment this line if you want to keep your existing rules.
    await setRules(rules, token);
  } catch (e) {
    console.error(e);
    process.exit(-1);
  }

   	// Listen to the stream.
	  // This reconnection logic will attempt to reconnect when a disconnection is detected.
	  // To avoid rate limites, this logic implements exponential backoff, so the wait time
    // will increase if the client cannot reconnect to the stream.

  //Inside streamConnect, sequelize inserts to Database
	const stream = streamConnect(token);
	  let timeout = 0;
		stream.on('timeout', () => {
		    // Reconnect on error
		    console.warn('A connection error occurred. Reconnecting…');
		    setTimeout(() => {
		      timeout++;
		      streamConnect(token);
		    }, 2 ** timeout);
		    streamConnect(token);
		  });


	//res.status(200).json(stream);
		

	})();

})

//This will be used later for manual input from postman, or HTML input form
app.post('/tweets', async (req, res) => {
	try{
		if (req.query.bulk && req.query.bulk == 'on'){
			await Tweet.bulkCreate(req.body)
			res.status(201).json({message : 'created'})
		}
		else{
			await Tweet.create(req.body)
			res.status(201).json({message : 'created'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

//Get specific tweet from database by id
app.get('/tweets/:id', async (req, res) => {
	try{
		let tweet = await Tweet.findById(req.params.id)
		if (tweet){
			res.status(200).json(tweet)
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

//Insert with specific ID
app.put('/tweets/:id', async (req, res) => {
	try{
		let tweet = await Tweet.findById(req.params.id)
		if (tweet){
			await tweet.update(req.body)
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

//Delete specific ID
app.delete('/tweets/:id', async (req, res) => {
	try{
		let tweet = await Tweet.findById(req.params.id)
		if (tweet){
			await tweet.destroy()
			res.status(202).json({message : 'accepted'})
		}
		else{
			res.status(404).json({message : 'not found'})
		}
	}
	catch(e){
		console.warn(e)
		res.status(500).json({message : 'server error'})
	}
})

//Start server on port  
app.listen(8080)